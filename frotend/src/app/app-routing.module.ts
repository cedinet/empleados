import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'home',
        loadChildren: () => import('./home/home.module').then( m => m.HomeModule)
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'create-employee',
        loadChildren: () => import('./modals/create-employee/create-employee.module').then( m => m.CreateEmployeeModule)
    },
    {
        path: 'confirmation',
        loadChildren: () => import('./modals/confirmation/confirmation.module').then( m => m.ConfirmationModule)
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
