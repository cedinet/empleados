import { catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Employee } from '../data/Employee';

@Injectable({
    providedIn: 'root'
})
export class EmployeeService {

    urlApi = environment.api;

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        }),
    };

    constructor(private http: HttpClient) {}

    setEmployee(employee: Employee): Observable<any> {
        let urlFinal = this.urlApi + '/register';

        return this.http.post<Employee>(urlFinal, JSON.stringify(employee), this.httpOptions)
        .pipe(retry(1), catchError(this.errorHandl));
    }

    deleteEmployee(id: number) {
        let urlFinal = this.urlApi + '/delete';
        let params = new HttpParams();
        params = params.append('idEmployee', id);

        return this.http.delete<number>(urlFinal, { params: params }).
        pipe(retry(1), catchError(this.errorHandl));
    }

    getEmployees(): Observable<any> {
        let urlFinal = this.urlApi + '/consult';

        return this.http.get<any>(urlFinal).
        pipe(retry(1), catchError(this.errorHandl));
    }

    errorHandl(error: any) {
        let errorMessage = '';

        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        } else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }

        return throwError(() => {
            return errorMessage;
        });
    } 

}