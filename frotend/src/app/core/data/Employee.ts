export class Employee {

    firstName?: String = '';
	lastName?: String = '';
	otherNames?: String = '';
	country?: String = '';
	email?: String = '';

}
