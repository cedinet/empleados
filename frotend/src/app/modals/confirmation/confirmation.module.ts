import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ConfirmationRoutingModule } from './confirmation-routing.module';
import { ConfirmationComponent } from './confirmation.component';
import { CustomMaterialModule } from 'src/app/core/material-module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ConfirmationRoutingModule,
        CustomMaterialModule
    ],
    declarations: [ConfirmationComponent]
})
export class ConfirmationModule {}
