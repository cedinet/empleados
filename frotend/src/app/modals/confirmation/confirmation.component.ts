import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-confirmation',
    templateUrl: './confirmation.component.html',
    styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

    constructor(public dialogRef: MatDialogRef<ConfirmationComponent>) {}

    onNoClick(): void {
        this.dialogRef.close();
    }

    ngOnInit() {
        
    }

}
