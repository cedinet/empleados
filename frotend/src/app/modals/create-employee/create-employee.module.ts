import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateEmployeeRoutingModule } from './create-employee-routing.module';
import { CreateEmployeeComponent } from './create-employee.component';
import { CustomMaterialModule } from 'src/app/core/material-module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CreateEmployeeRoutingModule,
        CustomMaterialModule
    ],
    declarations: [CreateEmployeeComponent]
})
export class CreateEmployeeModule {}
