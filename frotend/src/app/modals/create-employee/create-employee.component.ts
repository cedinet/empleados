import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Employee } from 'src/app/core/data/Employee';
import { EmployeeService } from 'src/app/core/services/employee.service';

@Component({
    selector: 'app-create-employee',
    templateUrl: './create-employee.component.html',
    styleUrls: ['./create-employee.component.scss'],
})
export class CreateEmployeeComponent implements OnInit {

    formGroup: any = {};
    employee = new Employee();

    constructor(public dialogRef: MatDialogRef<CreateEmployeeComponent>, private formBuilder: FormBuilder,
        private employeeService: EmployeeService) {}
    
    onNoClick(): void {
        this.dialogRef.close();
    }

    ngOnInit() {
        this.formGroup = this.formBuilder.group({
            firstName: ['', [ Validators.required, Validators.pattern('^[A-Z ]*$') ]],
            lastName: ['', [ Validators.required, Validators.pattern('^[A-Z ]*$') ]],
            otherNames: ['', [ Validators.pattern('^[A-Z ]*$') ]],
            country: [ 'co' ]
        });
    }

    createEmployee() {
        if (this.formGroup.valid) {
            this.employee.firstName = this.formGroup.value.firstName;
            this.employee.lastName = this.formGroup.value.lastName;
            this.employee.otherNames = this.formGroup.value.otherNames;
            this.employee.country = this.formGroup.value.country;
            
            this.employeeService.setEmployee(this.employee).subscribe(_ => {
                this.dialogRef.close();
            });
        }
    }

}
