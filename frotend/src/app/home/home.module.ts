import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home.component';

import { HomeRoutingModule } from './home-routing.module';
import { CustomMaterialModule } from '../core/material-module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HomeRoutingModule,
        CustomMaterialModule
    ],
    declarations: [HomeComponent]
})
export class HomeModule {}
