import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EmployeeService } from '../core/services/employee.service';
import { ConfirmationComponent } from '../modals/confirmation/confirmation.component';
import { CreateEmployeeComponent } from '../modals/create-employee/create-employee.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss'],
})
export class HomeComponent implements OnInit {

    resultsLength = 0;
    displayedColumns: string[] = ['firstName', 'otherNames', 'lastName', 'email', 'acciones'];
    employees = [];

    constructor(private employeeService: EmployeeService, public dialog: MatDialog) {}

    ngOnInit() {
        this.getEmployees();
    }

    getEmployees() {
        return this.employeeService.getEmployees().subscribe((data: []) => {
            for (let employee in data) {
                this.employees = data[employee];
            }

            this.resultsLength = this.employees.length;
        });
    }

    createDialog(): void {
        const dialogRef = this.dialog.open(CreateEmployeeComponent, {
            width: '600px'
        });

        dialogRef.afterClosed().subscribe(_ => {
            this.getEmployees();
        });
    }

    deleteDialog(ele: any): void {
        const dialogRef = this.dialog.open(ConfirmationComponent, {});

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.employeeService.deleteEmployee(ele.id).subscribe(_ => {
                    this.getEmployees();
                })
            }
        });
    }

}
