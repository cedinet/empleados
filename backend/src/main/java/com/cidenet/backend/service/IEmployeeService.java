package com.cidenet.backend.service;

import org.springframework.http.ResponseEntity;

import com.cidenet.backend.dto.EmployeeDto;

public interface IEmployeeService {

	public ResponseEntity<Object> registerEmployee(EmployeeDto employeeDto);
	
	public ResponseEntity<Object> deleteEmployee(long idEmployee);
	
	public ResponseEntity<Object> consultEmployees();
	
}
