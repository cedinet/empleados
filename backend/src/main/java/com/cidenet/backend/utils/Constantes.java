package com.cidenet.backend.utils;

public class Constantes {

	public static final String MAIL_DOMAIN = "@cidenet.com";
	public static final String POINT = ".";
	public static final String URL_WEB = "http://localhost:4200";
	
	public static final String MESSAGE = "mensaje";
	public static final String MESSAGE_CREATED = "Empleado creado";
	public static final String MESSAGE_DELETED = "Empleado eliminado";
	public static final String MESSAGE_ERROR = "Error eliminando empleado";
	
	public static final String EXPRESION_REGULAR_EMPTY = "\\s+";
	
}
