package com.cidenet.backend.repository;

import org.springframework.data.repository.CrudRepository;

import com.cidenet.backend.entitys.Employee;

public interface IEmployeeRepository extends CrudRepository<Employee, Long> {

}
