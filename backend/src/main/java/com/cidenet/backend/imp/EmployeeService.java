package com.cidenet.backend.imp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.cidenet.backend.dto.EmployeeDto;
import com.cidenet.backend.entitys.Employee;
import com.cidenet.backend.repository.IEmployeeRepository;
import com.cidenet.backend.service.IEmployeeService;
import com.cidenet.backend.utils.Constantes;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class EmployeeService implements IEmployeeService {

	private IEmployeeRepository iEmployeeRepository;
	
	@Autowired
	EmployeeService(IEmployeeRepository iEmployeeRepository) {
        this.iEmployeeRepository = iEmployeeRepository;
    }
	
	@Override
	public ResponseEntity<Object> registerEmployee(EmployeeDto employeeDto) {
		Map<String, String> response = new HashMap<>();
		
		Employee newEmployee = this.createEmployee(employeeDto);
		this.buildMailEmployee(newEmployee.getId());
		
		response.put("id", String.valueOf(newEmployee.getId()));
		response.put(Constantes.MESSAGE, Constantes.MESSAGE_CREATED);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@Override
	public ResponseEntity<Object> deleteEmployee(long idEmployee) {
		
		Map<String, String> response = new HashMap<>();
		response.put("id", String.valueOf(idEmployee));
		
		if (iEmployeeRepository.existsById(idEmployee)) {
			iEmployeeRepository.deleteById(idEmployee);
			response.put(Constantes.MESSAGE, Constantes.MESSAGE_DELETED);
		} else {
			response.put(Constantes.MESSAGE, Constantes.MESSAGE_ERROR);
		}
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@Override
	public ResponseEntity<Object> consultEmployees() {
		
		Map<String, List<Employee>> response = new HashMap<>();
		List<Employee> employees = (List<Employee>) iEmployeeRepository.findAll();
		
		response.put("employees", employees);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	public Employee createEmployee(EmployeeDto employeeDto) {
		return iEmployeeRepository.save(new Employee(
				employeeDto.getFirstName(),
				employeeDto.getLastName(),
				employeeDto.getOtherNames(),
				employeeDto.getCountry(),
				employeeDto.getEmail()));
	}
	
	public Employee buildMailEmployee(Long id) {
		Employee employee = iEmployeeRepository.findById(id).get();
		employee.setFirstName(employee.getFirstName());
		employee.setLastName(employee.getLastName());
		employee.setOtherNames(employee.getOtherNames());
		employee.setCountry(employee.getCountry());
		employee.setEmail(employee.getFirstName().replaceAll(Constantes.EXPRESION_REGULAR_EMPTY, "")
				.concat(Constantes.POINT)
				.concat(employee.getLastName().replaceAll(Constantes.EXPRESION_REGULAR_EMPTY, ""))
				.concat(Constantes.POINT)
				.concat(id.toString())
				.concat(Constantes.MAIL_DOMAIN)
				.concat(Constantes.POINT).
				concat(employee.getCountry()));
		
		return iEmployeeRepository.save(employee);
	}
	
}
