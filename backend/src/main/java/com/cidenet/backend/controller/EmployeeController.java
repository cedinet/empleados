package com.cidenet.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.backend.dto.EmployeeDto;
import com.cidenet.backend.service.IEmployeeService;
import com.cidenet.backend.utils.Constantes;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	private IEmployeeService iEmployeeService;
	
	@Autowired
    public EmployeeController(IEmployeeService iEmployeeService) {
        this.iEmployeeService = iEmployeeService;
    }
	
	@CrossOrigin(origins = Constantes.URL_WEB, methods = { RequestMethod.POST })
	@PostMapping(path="/register", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> registerEmployee(@RequestBody EmployeeDto employeeDto) {
		
		return iEmployeeService.registerEmployee(employeeDto);
    }
	
	@CrossOrigin(origins = Constantes.URL_WEB, methods = { RequestMethod.DELETE })
	@DeleteMapping(path="/delete", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteEmployee(@RequestParam long idEmployee) {
		
		return iEmployeeService.deleteEmployee(idEmployee);
    }
	
	@CrossOrigin(origins = Constantes.URL_WEB, methods = { RequestMethod.GET })
	@GetMapping(path="/consult", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> consultEmployees() {
		
		return iEmployeeService.consultEmployees();
    }
	
}
