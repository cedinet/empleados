package com.cidenet.backend.entitys;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="employee")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="other_names")
	private String otherNames;
	
	@Column(name="country")
	private String country;
	
	@Column(name="email")
	private String email;
	
	public Employee() {
		
	}

	public Employee(long id, String firstName, String lastName, String otherNames, String country, String email) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.otherNames = otherNames;
		this.country = country;
		this.email = email;
	}
	
	public Employee(String firstName, String lastName, String otherNames, String country, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.otherNames = otherNames;
		this.country = country;
		this.email = email;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOtherNames() {
		return otherNames;
	}

	public void setOtherNames(String otherNames) {
		this.otherNames = otherNames;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
