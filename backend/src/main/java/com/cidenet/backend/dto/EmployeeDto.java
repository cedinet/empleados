package com.cidenet.backend.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;

public class EmployeeDto {

	@Size(max = 20)
	private String firstName;
	
	@Size(max = 20)
	private String lastName;
	
	@Size(max = 50)
	private String otherNames;
	
	private String country;
	
	@Email
	private String email;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOtherNames() {
		return otherNames;
	}

	public void setOtherNames(String otherNames) {
		this.otherNames = otherNames;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
