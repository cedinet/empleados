package com.cidenet.backend;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureMockMvc
public class EmployeeTest {

	@Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;
    
    @Test
    public void crearEmpleado() throws Exception {

        mvc.perform(
                MockMvcRequestBuilders.post("/employee/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new CreateEmployee("JUAN", "PEREZ", "CARLOS", "co", ""))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andReturn();

        mvc.perform(MockMvcRequestBuilders
                .get("/employee/consult")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }
    
    @Test
    public void crearEmpleadoConUnMismoNombreYApellidoExistente() throws Exception {

        mvc.perform(
                MockMvcRequestBuilders.post("/employee/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new CreateEmployee("JUAN", "PEREZ", "CARLOS", "co", "")))
                        .content(objectMapper.writeValueAsString(new CreateEmployee("JUAN", "PEREZ", "DAVID", "co", ""))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andReturn();
        
        mvc.perform(MockMvcRequestBuilders
                .get("/employee/consult")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }
    
    @Test
    public void crearEmpleadoConServicioEnEstadosUnidos() throws Exception {

        mvc.perform(
                MockMvcRequestBuilders.post("/employee/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new CreateEmployee("JUAN", "PEREZ", "ANDRES", "us", ""))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andReturn();
        
        mvc.perform(MockMvcRequestBuilders
                .get("/employee/consult")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }
    
    @Test
    public void crearEmpleadoValidarQueHayaSidoCreadoYElimarlo() throws Exception {

    	MvcResult resultadoCrearEmpleado = mvc.perform(
                MockMvcRequestBuilders.post("/employee/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new CreateEmployee("ANDRES", "RAMOS", "DAVID", "co", ""))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andReturn();
    	
		mvc.perform(MockMvcRequestBuilders
                .get("/employee/consult")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
		
		CreateEmployeeResult resultado = objectMapper.readValue(resultadoCrearEmpleado.getResponse().getContentAsString(), CreateEmployeeResult.class);
        
		mvc.perform(MockMvcRequestBuilders
                .delete("/employee/delete")
                .param("idEmployee", resultado.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }
	
}
