package com.cidenet.backend;

public class CreateEmployee {
	
	private String firstName;
	private String lastName;
	private String otherNames;
	private String country;
	private String email;
	
	public CreateEmployee(String firstName, String lastName, String otherNames, String country, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.otherNames = otherNames;
		this.country = country;
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getOtherNames() {
		return otherNames;
	}

	public String getCountry() {
		return country;
	}

	public String getEmail() {
		return email;
	}
	
}
